<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
	<?php include("includes/include_css.html") ?>
	<style>
		//	modal pop up css
	.modal_box {
		position: absolute;
		z-index: 10000;
		/* 1 */
		top: 0;
		left: 0;
		/*			visibility: hidden;*/
		width: 100%;
		height: 100%;
	}

	.modal_box.is-visible {
		visibility: visible;
	}

	.modal_box-overlay {
		position: fixed;
		z-index: 10;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;

		background: hsla(0, 0%, 0%, 0.5);
		/*			visibility: hidden;
			opacity: 0;
			transition: visibility 0s linear 0.3s, opacity 0.3s;
*/
	}

	.modal_box.is-visible .modal_box-overlay {
		opacity: 1;
		visibility: visible;
		transition-delay: 0s;
	}

	.modal_box-wrapper {
		position: absolute;
		z-index: 9999;
		top: 50%;
		left: 50%;
		width: 40%;
		transform: translate(-50%, -50%);
		background-color: #fff;
		box-shadow: 0 0 1.5em hsla(0, 0%, 0%, 0.35);
	}

	.modal_box-transition {
		/*
			transition: all 0.3s 0.12s;
			transform: translateY(-10%);
			opacity: 0;
*/
	}

	.modal_box.is-visible .modal_box-transition {
		/*
			transform: translateY(0);
			opacity: 1;
*/
	}

	.modal_box-header,
	.modal_box-content {
		padding: 1em;
	}

	.modal_box-header {
		position: relative;
		background-color: #fff;
	}

	.modal_box-close {
		position: absolute;
		top: 0;
		right: 0;
		padding: 1em;
		color: #000;
		font-weight: 600;
		background: none;
		border: 0;
	}



	.modal_box-heading {
		font-size: 1.125em;
		margin: 0;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	}

	.modal_box-content > *:first-child {
		margin-top: 0;
	}

	.modal_box-content > *:last-child {
		margin-bottom: 0;
	}

	.modal_box-content p {
		color: #000;
		font-weight: 600;
		text-align: center;
	}
	</style>
</head>

<body>
	<!--loader-->
	<div class="page-loader">
		<img src="dist/img/page-loader.gif" class="img-responsive loader">
	</div>
	<!--loader ends-->
	<?php include("includes/browser_upgrade.html") ?>
	<?php include("includes/header.html") ?>
	<?php include("includes/sidebar.html") ?>
	<!--  insert body content  -->

	<section id="contact" class="main_wrapper">
		<div class="container">
			<div class="heading text-center">
				<h2>contact <span>us</span></h2>
			</div>

			<div class="form_wrapper">
				<!-- <img class="earings" src="dist/img/contact_ear.png"> -->

				<div class="address_box">
					<p>
						<img src="dist/img/map.png">
						<span><a href="https://www.google.com/maps/place/Rosentiques/@18.963135,72.808163,15z/data=!4m5!3m4!1s0x0:0xcc82bfb62d0caa8f!8m2!3d18.963135!4d72.808163" target="_blank"><strong>Rosentiques Fine Jewellery</strong><br>Office No. 6/8, 1st floor,<br>Delstar Building, N.S. Patkar Marg,<br>Kemps Corner, Next to Crossword Bookstore,<br>Mumbai 400 036</a></span>

					</p>
					<p class="space-bottom">
						<img src="dist/img/call.png">
						<span><a href="tel:+912249250000">+912249250000</a></span>
					</p>
					<p class="space-bottom">
						<img src="dist/img/email.png" style="height: 18px;">

						<span><a href="mailto:info@rosentiques.com" target="_top">info@rosentiques.com</a></span>
					</p>
					<!-- <p>
  							<img src="dist/img/url.png">
  							<span><a href="www.rosentiques.com" target="_blank"> www.rosentiques.com</a></span>
  						</p> -->
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-5">

						<div class="item">
							<img src="dist/img/Amolya/center-6.png" class="img-responsive" />
						</div>

					</div>
					<div class="col-xs-12 col-sm-7">
						<div class="form">
							<form id="contact-form" method="post" name="google-sheet">
								<div class="submit_error_result"></div>
								<div class="input-field col-xs-12">
									<input id="name" name="name" type="text">
									<label for="name">First Name</label>
								</div>
								<div class="input-field col-xs-12">
									<input id="last_name" name="last_name" type="text">
									<label for="last_name">Last Name</label>
								</div>
								<div class="input-field col-xs-12">
									<input id="contact_number" name="contact_number" type="tel">
									<label for="contact_number">Contact Number</label>
								</div>
								<div class="input-field col-xs-12">
									<input id="email" name="email" type="email">
									<label for="email">Email Address</label>
								</div>
								<div class="input-field col-xs-12">
									<textarea id="message" name="msg" class="materialize-textarea"></textarea>
									<label for="message">Message</label>
								</div>
								<div class="input-field col-xs-12">
									<button class="btn rounded-btn contactBtn modal-toggle" type="submit" value="send">send</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-10 col-sm-offset-1">

				<ul class="list-inline text-center affiliat_imges">
					<li><span>ASSOCIATED WITH</span></li>
					<li><img src="dist/img/contact-affiliated/GJEPC.png" class="img-responsive"></li>
					<li><img src="dist/img/contact-affiliated/GJC.png" class="img-responsive"></li>
					<li><img src="dist/img/contact-affiliated/Pt-Logo.png" class="img-responsive"></li>
					<li><img src="dist/img/contact-affiliated/IBJA.png" class="img-responsive"></li>

				</ul>

			</div>
		</div>


		<div class="modal_box hide" id="modal_box">
			<div class="modal_box-overlay modal-toggle"></div>
			<div class="modal_box-wrapper modal_box-transition">
				<div class="modal_box-header">
					<button class="modal_box-close modal-toggle">
						X</button>
				</div>

				<div class="modal_box-body">
					<div class="modal_box-content">
						<p>Thank you for writing to us.</p>
						<p>We will get back to you shortly.</p>
					</div>
				</div>
			</div>
		</div>
	</section>



	<?php include("includes/footer.html") ?>
	<?php include("includes/include_js.html") ?>

	<script>
		//contact page form validation
		$(".contactBtn").click(function() {
			//			var formData = $("#contact-form").serialize();

			// alert(formData);

		});
		$("#contact-form").validate({
			errorElement: 'span',
			rules: {
				name: {
					required: true,
				},
				last_name: {
					required: true
				},
				contact_number: {
					required: true
				},
				email: {
					required: true
				},
				msg: {
					required: true
				}
			},
			messages: {
				name: "Please enter your firstname",
				last_name: "Please enter your lastname",
				contact_number: "Please provide a contact details",
				email: "Please enter a valid email address",
				msg: "Please provide details"
			},
			submitHandler: function(form) {
				$.ajax({
					url: 'https://script.google.com/macros/s/AKfycbxzKFiBS8LOG44AAfEqPdQKRsSxiq2yIWtiNvH0KroZUpO4VVmoOVjQvHYitg5f30Or6Q/exec',
					type: 'post',
					data: $("#contact-form").serializeArray(),
					success: function($response) {
						$('#modal_box').removeClass('hide');
						$('body').css({
							"overflow": "hidden",
							"position": "relative"
						});
						$('#contact-form')[0].reset();
						setTimeout(function() {
							$('#modal_box').addClass('hide');
							$('body').css({
								"overflow": "unset",
								"position": "unset"
							});
						}, 5000);
					},
					error: function() {
						alert("There was an error. PLease try again.")
					}
				});
			}

		});
		$(".modal_box-close").click(function() {
			$("#modal_box").hide();
		});

	</script>


</body>

</html>
