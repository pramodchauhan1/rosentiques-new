<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

  	<section id="overview" class="main_wrapper">
  		<div class="container">
  				<div class="heading text-center">
  					<h2>overview </h2>
             
  				</div>

          <div class="col-sm-12 col-md-10 col-md-offset-1">
            
            <div class="col-xs-12 col-sm-6">
              <div class="desc">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                <p>
                  There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
                </p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6">
              <div class="col-xs-12 overview_1">
                <div class="row">
                  <img src="dist/img/overview_1.png" class="img-responsive">
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 no-padd overview_2">
                  <img src="dist/img/overview_2.png" class="img-responsive">
              </div>
              <div class="col-xs-12 col-sm-6 no-padd overview_3">
                  <img src="dist/img/overview_3.png" class="img-responsive">
              </div>
            </div>
          </div>
  		</div>
      <img src="dist/img/overview_diamond.png" class="diamond">
  	</section>



    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>


</body>

</html>
