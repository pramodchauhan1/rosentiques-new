<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>

    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="thank-you" class="thank-you">
        <div class="content">
            <div class="heading">
                <h2 class="text-center">Thank You !</h2>
            </div>
            <p class="text-center">Thank you for getting in touch with us. We will contact you shortly.</p>
            <p class="text-center top-space"><a href="index.php" role="button" class="btn rounded-btn contactBtn">BACK
                    TO HOME</a></p>
        </div>
    </section>

    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>


</body>

</html>