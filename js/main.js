/*  ==========================================================================
  Author: Mvishal Shukla
  Updated: --/--/--
  Notes: Hand Coded JS.
  ========================================================================== */

//add footer to bottom
function footerAlign() {
    var footerHeight = $("footer").outerHeight();
    var bodyHeight = $(".main_wrapper").outerHeight();
    $(".main_wrapper").css("padding-bottom", footerHeight);
    // $("footer").css("height", footerHeight);
}


//banner height
var winHeight = $(window).height();
var winWidth = $(window).width();
var bannerHeight = $("#banner").height();
var setBannerHeight = bannerHeight / 2;
$(".banner_section1").css("height", setBannerHeight);
$(".banner_section2").css("height", setBannerHeight);

//footer function


//window on load

$(window).on('load', function () {
    $(".page-loader").fadeOut(2000);

    var headerHeight = $("#slider_wrapper .frontpage_logo").outerHeight();
    var diff = winHeight - headerHeight - $("#slider_wrapper .slider_details").outerHeight();
    var sWidth = $("#slider_wrapper .split-slideshow").outerWidth();


    var sum = headerHeight + $("#slider_wrapper .slider_details").outerHeight();
    var difference = $(window).outerHeight() - sum;

    if (winWidth < winHeight && winWidth < 767) {
        var setHeight = sWidth * 2.29;

    } else {
        var setHeight = sWidth / 2.29;
    }


    if (setHeight > difference) {
        $("#slider_wrapper .split-slideshow").outerHeight(difference);
        $("#slider_wrapper .slider .item").outerHeight(difference);
        $("#slider_wrapper .slider .item").outerWidth(sWidth);
        var getImgWidth = $("#slider_wrapper .slider .item img").outerWidth();
        $(".slider_details").css('width', getImgWidth);
        // console.log("difference");
    } else {
        $("#slider_wrapper .split-slideshow").outerHeight(setHeight);
        $("#slider_wrapper .slider .item").outerHeight(setHeight);
        $("#slider_wrapper .slider .item").outerWidth(sWidth);
        var getImgWidth = $("#slider_wrapper .slider .item img").outerWidth();
        $(".slider_details").css('width', getImgWidth);
        var centerDiff = headerHeight + $("#slider_wrapper .slider_details").outerHeight() + $("#slider_wrapper .split-slideshow").outerHeight();
        var setMargin = $(window).outerHeight() - centerDiff;
        $("#slider_wrapper .split-slideshow").css({
            "margin-top": setMargin / 2
        });
        $("#slider_wrapper .slider_details").css({
            "margin-bottom": setMargin / 2
        });

    }
    // Homepage Slider Ends

});

if (winWidth > 768) {
    $(window).load(function () {
        var b = $(".jewellery-content-image").outerHeight();
        $(".jewellery-content-image").outerHeight(b);
        $(".jewellery-desc").outerHeight(b);
    });
    $(window).load(function () {
        var a = $(".jewellery-content-image").innerHeight();
        $(".jewellery-desc").innerHeight(a);
    });
}

$(document).ready(function () {
    //initializing wow slider
    new WOW().init();

    if (winWidth > 768) {
        footerAlign();
    }
    if (winWidth > 767) {
        $('.collection_list').lightGallery({
            selector: '.collectionModal',
            thumbnail: false,
            share: false,
            // zoom:false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            subHtmlSelectorRelative: true,
        });
    }
    //menu
    $(".hamburge-menu").click(function () {

        var check_class = $(".hamburge-menu").hasClass("hamburge-open");
        if (!check_class) {
            $(this).addClass("hamburge-open");
            $(".menuBar").addClass("slideMenu");
            $(".menu-text").text("close");
        } else {
            $(this).removeClass("hamburge-open");
            $(".menuBar").removeClass('slideMenu');
            $(".menu-text").text("menu");
        }
    });

    //menu on hover change image
    $(".menu-product .menu-product-img").hide();
    $(".menu-product .menu-deafult-img").show();
    // $('#menuProduct li')
    //     .on('mouseover', function() {
    //         $(".menu-product .menu-deafult-img").hide();
    //         $(".menu-product .menu-product-img").show();
    //         var product_name = $(this).find("a").attr("data-img");
    //         var newsrc= "dist/img/"+product_name;
    //         $(".menu-product .menu-product-img").attr("src", newsrc);

    //     })
    //     .on('mouseout', function() {
    //         $(".menu-product .menu-product-img").hide();
    //         $(".menu-product .menu-deafult-img").show();
    //     });

    //homepage banner
    $("body.frontPage_body").css("overflow", "hidden");

    $("#banner").bind("mousewheel", function () {
        return false;
    });

    //Milestone page Timeline equal-height
    $('.timeline .timeline_img , .timeline .timeline_details').equalHeights();


    //homepage scroll to down
    $("a.scroll_down").on('click', function (event) {
        $("body.frontPage_body").css("overflow", "hidden");
        $("#banner").addClass("slide-up-banner");
        $('html, body').animate({
            scrollTop: winHeight
        }, 2000, function () {});
    });

    //contact page slider
    $('#contact_slider').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        items: 1
    });

    // Nazaqat , Amolya, Polki, Platinum Page
    $('.collectionGallery').slick({
        dots: false,
        infinite: true,
        speed: 600,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $('a.collectionModal[data-slide]').click(function (e) {
        e.preventDefault();
        var slideno = $(this).data('slide');
        $('.collectionGallery').slick('slickGoTo', slideno - 1);
    });

    $('.modal').on('shown.bs.modal', function (e) {
        $('.slick-slider').resize();
    });


    //Newsletter
    $('.newsSlider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
                breakpoint: 1199,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    //media Corner
    $('.mediaCorner').slick({
        dots: false,
        infinite: true,
        speed: 600,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        rows: 0
    });

    $('a.media_item[data-slide]').click(function (e) {
        e.preventDefault();
        var slideno = $(this).data('slide');
        $('.mediaCorner').slick('slickGoTo', slideno - 1);
    });

    $('.modal').on('shown.bs.modal', function (e) {
        $('.mediaCorner').resize();
    });


    //spolight loadmore
    $(".show_grid").slice(0, 1).show();
    $(".loadMore").on('click', function (e) {
        e.preventDefault();
        $(".show_grid:hidden").slice(0, 2).slideDown();
        if ($(".show_grid:hidden").length == 0) {
            // $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1000);
    });

    $(".items").on("contextmenu", function (e) {
        return false;
    });
});

//window scroll
$(window).scroll(function () {
    if ($(window).scrollTop() >= 100) {
        $('header').addClass('fixed-header');
    } else {
        $('header').removeClass('fixed-header');
    }

});



//slide home banner
function slideBanner() {
    setTimeout(function () {
        $("body.frontPage_body").css("overflow", "hidden");
        $("#banner").addClass("slide-up-banner")
        $('html, body').animate({
            scrollTop: winHeight
        }, 1000, function () {});
    }, 3500);
}



// Homepage Slider Starts
var $slider = $('.slideshow .slider'),
    maxItems = $('.item', $slider).length,
    dragging = false,
    tracking,
    rightTracking;

$sliderRight = $('.slideshow').clone().addClass('slideshow-right').appendTo($('.split-slideshow'));

rightItems = $('.item', $sliderRight).toArray();
reverseItems = rightItems.reverse();
$('.slider', $sliderRight).html('');
for (i = 0; i < maxItems; i++) {
    $(reverseItems[i]).appendTo($('.slider', $sliderRight));
}

$slider.addClass('slideshow-left');
$('.slideshow-left').slick({
    vertical: true,
    verticalSwiping: true,
    arrows: false,
    infinite: true,
    dots: true,
    speed: 1000,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)'
}).on('beforeChange', function (event, slick, currentSlide, nextSlide) {

    if (currentSlide > nextSlide && nextSlide == 0 && currentSlide == maxItems - 1) {
        $('.slideshow-right .slider').slick('slickGoTo', -1);
        $('.slideshow-text').slick('slickGoTo', maxItems);
        $('.slideshow-number').slick('slickGoTo', maxItems);

    } else if (currentSlide < nextSlide && currentSlide == 0 && nextSlide == maxItems - 1) {
        $('.slideshow-right .slider').slick('slickGoTo', maxItems);
        $('.slideshow-text').slick('slickGoTo', -1);
        $('.slideshow-number').slick('slickGoTo', -1);
    } else {
        $('.slideshow-right .slider').slick('slickGoTo', maxItems - 1 - nextSlide);
        $('.slideshow-text').slick('slickGoTo', nextSlide);
        $('.slideshow-number').slick('slickGoTo', nextSlide);
    }
}).on("mousewheel", function (event) {
    event.preventDefault();
    if (event.deltaX > 0 || event.deltaY < 0) {
        $(this).slick('slickNext');
    } else if (event.deltaX < 0 || event.deltaY > 0) {
        $(this).slick('slickPrev');
    };
}).on('mousedown touchstart', function () {
    dragging = true;
    tracking = $('.slick-track', $slider).css('transform');
    tracking = parseInt(tracking.split(',')[5]);
    rightTracking = $('.slideshow-right .slick-track').css('transform');
    rightTracking = parseInt(rightTracking.split(',')[5]);
}).on('mousemove touchmove', function () {
    if (dragging) {
        newTracking = $('.slideshow-left .slick-track').css('transform');
        newTracking = parseInt(newTracking.split(',')[5]);
        diffTracking = newTracking - tracking;
        $('.slideshow-right .slick-track').css({
            'transform': 'matrix(1, 0, 0, 1, 0, ' + (rightTracking - diffTracking) + ')'
        });
    }
}).on('mouseleave touchend mouseup', function () {
    dragging = false;
});

$('.slideshow-right .slider').slick({
    swipe: false,
    vertical: true,
    arrows: false,
    infinite: true,
    speed: 950,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    initialSlide: maxItems - 1
});
$('.slideshow-text,.slideshow-number').slick({
    swipe: false,
    vertical: true,
    arrows: false,
    infinite: true,
    speed: 900,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)'
});

$('.next_btn').click(function () {
    $(".slider").slick('slickNext');
});
