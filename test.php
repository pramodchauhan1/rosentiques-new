<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="collections" class="main_wrapper">
        <div class="container">
            <div class="img_wrap">
                <div class="col-xs-6">
                    <div class="row">
                        <img src="dist/img/Amolya/Banner1stHalf.jpg" class="img-responsive wow slideInDown" data-wow-delay="4s">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <img src="dist/img/Amolya/Banner2ndHalf.jpg" class="img-responsive wow slideInUp" data-wow-delay="4s">
                    </div>
                </div>
            </div>

            <div class="desc text-justify">
                <p>
                    The brand stands for the legacy that has been passed down over 4 generations, each one passing down its essence to the next generation which in turn evolves to add its own fervor thereby growing into a unique family matrix. </p>

                <p>Staying true to its name, in its launch year ‘the legacy of Amolya’ begins by celebrating the concept of ’priceless-ness'. Since there is nothing better that could represent 'Amolya' than 'the gift of life' itself 'The legacy of Amolya' kick starts for priceless jewelry collections that represent the 4 elements of nature that make 'life' possible on this beautiful and unique planet Earth. .one of its kind in the whole universe. </p>
            </div>

            <div class="heading text-center">
                <h2>our collections</h2>
            </div>

            <div class="col-xs-12 collection_list">
                <div id="light-gallery">
                    <div class="col-md-3 col-sm-3 col-xs-12 padd-lr-10">
                        <a href="img/Amolya/left-1.jpg" class="items">
                            <img src="dist/img/Amolya/left-1.jpg" class="img-responsive">
                        </a>
                        <!--
<div class="overlay">
    <a href="" class="collectionModal" data-toggle="modal" data-target="#collection_gallery" data-slide="1">
        <img src="dist/img/resize.png">
    </a>
</div>
-->
                        <div class="items mar-t-20">
                            <a href="">
                                <img src="dist/img/Amolya/left-2.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="" class="collectionModal" data-toggle="modal" data-target="#collection_gallery" data-slide="2">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items mar-t-20">
                            <a href="">
                                <img src="dist/img/Amolya/left-3.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items mar-t-20">
                            <a href="">
                                <img src="dist/img/Amolya/left-4.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items mar-t-20">
                            <a href="">
                                <img src="dist/img/Amolya/left-5.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items mar-t-20">
                            <a href="">
                                <img src="dist/img/Amolya/left-6.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items mar-t-20">
                            <a href="">
                                <img src="dist/img/Amolya/left-7.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>

                    </div>




                    <div class="col-md-6 col-sm-6 col-xs-12 no-lr-padd">
                        <div class="col-sm-6 col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-1.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-2.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-3.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="" class="collectionModal" data-toggle="modal" data-target="#collection_gallery" data-slide="3">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>








                        <!--                    rahul edit-->


                        <div class="col-sm-6 col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-4.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-5.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-6.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="" class="collectionModal" data-toggle="modal" data-target="#collection_gallery" data-slide="3">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-7.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-8.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-9.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="" class="collectionModal" data-toggle="modal" data-target="#collection_gallery" data-slide="3">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-10.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-11.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-12.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="" class="collectionModal" data-toggle="modal" data-target="#collection_gallery" data-slide="3">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-13.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6 col-xs-12 padd-lr-10">
                            <div class="items">
                                <a href="">
                                    <img src="dist/img/Amolya/center-14.jpg" class="img-responsive">
                                </a>
                                <div class="overlay">
                                    <a href="">
                                        <img src="dist/img/resize.png">
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12 padd-lr-10">
                        <div class="items">
                            <a href="">
                                <img src="dist/img/Amolya/right-1.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items">
                            <a href="">
                                <img src="dist/img/Amolya/right-2.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items">
                            <a href="">
                                <img src="dist/img/Amolya/right-3.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items">
                            <a href="">
                                <img src="dist/img/Amolya/right-4.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items">
                            <a href="">
                                <img src="dist/img/Amolya/right-5.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items">
                            <a href="">
                                <img src="dist/img/Amolya/right-6.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items">
                            <a href="">
                                <img src="dist/img/Amolya/right-7.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                        <div class="items">
                            <a href="">
                                <img src="dist/img/Amolya/right-8.jpg" class="img-responsive">
                            </a>
                            <div class="overlay">
                                <a href="">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>


                    </div>

                </div>
            </div>

        </div>
    </section>
    <div id="collection_gallery" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <div class="collectionGallery">
                        <div class="item slick-slide">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="dist/img/collection-modal1.jpg" class="img-responsive">
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="product_code_wrapper">
                                                <div class="product_code col-xs-12">
                                                    <span class="text-uppercase">product code:</span>
                                                    <span class="text-uppercase">4C2A8352</span>
                                                </div>
                                                <form class="product_code_details">
                                                    <div class="input-field col-xs-12 col-sm-11">
                                                        <input id="" type="text">
                                                        <label for="">First Name</label>
                                                    </div>
                                                    <div class="input-field col-xs-12 col-sm-11">
                                                        <input id="" type="email">
                                                        <label for="">Email</label>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <button class="btn rounded-btn" type="button">send</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>







                            <!--
                            <img src="dist/img/collection-modal1.jpg" class="img-responsive">
                            <div class="formWrapper col-xs-12">
                                <div class="row">
                                    <div class="product_code col-xs-12">
                                        <span class="text-uppercase">product code:</span>
                                        <span class="text-uppercase">4C2A8352</span>
                                    </div>
                                    <form class="product_code_details">
                                        <div class="input-field col-xs-12 col-sm-6">
                                            <input id="" type="text">
                                            <label for="">First Name</label>
                                        </div>
                                        <div class="input-field col-xs-12 col-sm-6">
                                            <input id="" type="email">
                                            <label for="">Email</label>
                                        </div>
                                        <div class="col-xs-12">
                                            <button class="btn rounded-btn" type="button">send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
-->
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/collection-modal2.png" class="img-responsive">
                            <div class="formWrapper col-xs-12">
                                <div class="row">
                                    <div class="product_code col-xs-12">
                                        <span class="text-uppercase">product code:</span>
                                        <span class="text-uppercase">4C2A8352</span>
                                    </div>
                                    <form class="product_code_details">
                                        <div class="input-field col-xs-12 col-sm-6">
                                            <input id="" type="text">
                                            <label for="">First Name</label>
                                        </div>
                                        <div class="input-field col-xs-12 col-sm-6">
                                            <input id="" type="email">
                                            <label for="">Email</label>
                                        </div>
                                        <div class="col-xs-12">
                                            <button class="btn rounded-btn" type="button">send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/collection-modal4.png" class="img-responsive">
                            <div class="formWrapper col-xs-12">
                                <div class="row">
                                    <div class="product_code col-xs-12">
                                        <span class="text-uppercase">product code:</span>
                                        <span class="text-uppercase">4C2A8352</span>
                                    </div>
                                    <form class="product_code_details">
                                        <div class="input-field col-xs-12 col-sm-6">
                                            <input id="" type="text">
                                            <label for="">First Name</label>
                                        </div>
                                        <div class="input-field col-xs-12 col-sm-6">
                                            <input id="" type="email">
                                            <label for="">Email</label>
                                        </div>
                                        <div class="col-xs-12">
                                            <button class="btn rounded-btn" type="button">send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

    <script type="text/javascript">
        //modal form 

        var length_value = $(".product_code_details :input").val().length;
        $(".product_code_details :input").focus(function() {

            $(this).parent().find('label').addClass('active');

        }).blur(function() {

            $(this).parent().find('label').removeClass("active");
            if ($(this).val().length > 0) {
                $(this).parent().find('label').addClass("active");
            } else {
                $(this).parent().find('label').removeClass("active");
            }
        });

    </script>


</body>

</html>
