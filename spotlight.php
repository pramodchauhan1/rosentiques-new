<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="spotlight" class="main_wrapper">
        <div class="container">
            <div class="heading text-center">
                <h2>spotlight &nbsp; <img class="ring" src="dist/img/ring.png"></h2>
            </div>

            <div class="col-xs-12 media_gallery">
                <div class="row show_grid">
                    <div class="col-sm-6">
                        <a href="#" class="main_img_Wrap">
                            <img src="dist/img/spotlight/1-big.jpg" alt="" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row show_grid">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <a href="#" class="main_img_Wrap">
                            <img src="dist/img/spotlight/1-big.jpg" alt="" class="img-responsive">
                        </a>
                    </div>
                </div>

                <div class="row show_grid">
                    <div class="col-sm-6">
                        <a href="#" class="main_img_Wrap">
                            <img src="dist/img/spotlight/1-big.jpg" alt="" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="#" class="main_img_Wrap">
                                    <img src="dist/img/spotlight/1-small.jpg" alt="" class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <a href="#" class="loadMore">
                    <div class="addbox">
                        <img src="dist/img/spotlight/add.png" alt="add">

                    </div>
                </a>

            </div>
        </div>
    </section>


    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>



</body>

</html>
