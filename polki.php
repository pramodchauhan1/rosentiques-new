<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="collections" class="main_wrapper">
        <div class="container">
            <div class="img_wrap">
                <div class="col-xs-6 hidden-xs">
                    <div class="row">
                        <img src="dist/img/Polki/Banner1stHalf.jpg" class="img-responsive wow slideInDown" data-wow-delay="2.5s">
                    </div>
                </div>
                <div class="col-xs-6 hidden-xs">
                    <div class="row">
                        <img src="dist/img/Polki/Banner2ndHalf.jpg" class="img-responsive wow slideInUp" data-wow-delay="2.5s">
                    </div>
                </div>
                <div class="col-xs-12 visible-xs">
                    <div class="row">
                        <img src="dist/img/mobile-polki.jpg" class="img-responsive wow slideInDown" data-wow-delay="2.5s">
                    </div>
                </div>
            </div>

            <div class="desc text-justify">
                <p>
                    The brand stands for the legacy that has been passed down over 4 generations, each one passing down its essence to the next generation which in turn evolves to add its own fervor thereby growing into a unique family matrix. </p>

                <p>Staying true to its name, in its launch year ‘the legacy of Amolya’ begins by celebrating the concept of ’priceless-ness'. Since there is nothing better that could represent 'Amolya' than 'the gift of life' itself 'The legacy of Amolya' kick starts for priceless jewelry collections that represent the 4 elements of nature that make 'life' possible on this beautiful and unique planet Earth. .one of its kind in the whole universe. </p>
            </div>

            <div class="heading ">
                <!-- <h2>our collections</h2> -->
                <p>When state-of-the art technologies, meticulous craftsmanship, and an eye for precision come together to create elegant pieces of jewellery, the result is Mukhlasi Polki Jewellery, by Rosentiques.<br><br>Mukhlasi is a range of fine jewellery fashioned through care for even the smallest nuance. That is why every finished piece is a masterpiece that’ll take your breath away in just a glance.<br><br>Mukhlasi brings you exquisite Indian jewellery finished in a contemporary fashion. Every design is marked by tradition and inspired by customs. The aim is always subtlety that reflects elegance. This is how we achieve enchanting results with Mukhlasi.</p>
            </div>

            <div class="col-xs-12 collection_list">
                <div class="col-md-3 col-sm-3 col-xs-12 padd-lr-10">
                    <!-- <div class="items">
                        <img src="dist/img/Polki/left-1.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/left-1.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> -->
                    <div class="items">
                        <img src="dist/img/Polki/left-2.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/left-2.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items mar-t-20">
                        <img src="dist/img/Polki/left-3.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/left-3.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items mar-t-20">
                        <img src="dist/img/Polki/left-4.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/left-4.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="items mar-t-20">
                        <img src="dist/img/Polki/left-5.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/left-5.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> -->
                    <!--
<div class="items mar-t-20">
    <img src="dist/img/Polki/left-6.jpg" class="img-responsive">
    <div class="overlay">
        <a class="collectionModal" data-src="dist/img/Polki/left-6.jpg">
            <img src="dist/img/resize.png">
        </a>
    </div>
</div>
-->
                    <!--
                    <div class="items mar-t-20">
                        <img src="dist/img/Polki/left-7.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/left-7.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items mar-t-20">
                        <img src="dist/img/Polki/left-8.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/left-8.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
-->
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 no-lr-padd">
                    <!--

                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-1.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-1.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-2.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-2.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
-->

                    <!-- <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-3.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-3.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div> -->
                    <!--
                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-4.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-4.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-5.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-5.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
-->

                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-6.png" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-6.png">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-7.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-7.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-8.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-8.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
-->

                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-9.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-9.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-10.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-10.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-11.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-11.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Polki/center-12.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Polki/center-12.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 padd-lr-10">
                    <!--
                    <div class="items">
                        <img src="dist/img/Polki/right-1.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-1.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
-->
                    <div class="items">
                        <img src="dist/img/Polki/right-2.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-2.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <!--
                    <div class="items">
                        <img src="dist/img/Polki/right-3.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-3.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
-->
                    <div class="items">
                        <img src="dist/img/Polki/right-4.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-4.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Polki/right-12.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-12.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Polki/right-13.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-13.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Polki/right-14.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-14.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <!--
                    <div class="items">
                        <img src="dist/img/Polki/right-5.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-5.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Polki/right-6.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-6.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Polki/right-7.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-7.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
-->
                    <!--
                    <div class="items">
                        <img src="dist/img/Polki/right-8.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-8.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Polki/right-9.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-9.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Polki/right-10.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Polki/right-10.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
-->
                    <!--
<div class="items">
    <img src="dist/img/Polki/right-11.jpg" class="img-responsive">
    <div class="overlay">
        <a class="collectionModal" data-src="dist/img/Polki/right-11.jpg">
            <img src="dist/img/resize.png">
        </a>
    </div>
</div>
-->
                </div>
            </div>


        </div>

        <div class="switch">
            <div class="container">
                <div class="switch-wrapper">
                    <p class="text-center">Switch to</p>

                    <div class="row">
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="nazaqat.php">Nazaqat</a></h2>
                            </div>
                        </div>
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="platinum.php">Platinum</a></h2>
                            </div>
                        </div>
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="amolya.php">Amolya</a></h2>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>



    <div id="collection_gallery" class="modal fade hide" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <div class="collectionGallery">
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/1.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/2.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/3.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/4.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/5.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/6.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/7.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/8.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/10.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/11.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/12.jpg" class="img-responsive">
                        </div>

                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/13.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/14.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/15.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/16.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/17.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/18.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/19.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/20.jpg" class="img-responsive">
                        </div>

                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/21.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/22.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/23.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/24.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/25.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/26.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/27.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/28.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/29.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/30.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/31.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Polki/pop-up/32.jpg" class="img-responsive">
                        </div>


                    </div>
                </div>

            </div>
        </div>

    </div>



    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

    <script type="text/javascript">
        //modal form 

        var length_value = $(".product_code_details :input").val().length;
        $(".product_code_details :input").focus(function() {

            $(this).parent().find('label').addClass('active');

        }).blur(function() {

            $(this).parent().find('label').removeClass("active");
            if ($(this).val().length > 0) {
                $(this).parent().find('label').addClass("active");
            } else {
                $(this).parent().find('label').removeClass("active");
            }
        });

    </script>



</body>

</html>
