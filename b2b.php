<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="collections" class="main_wrapper">
        <div class="container">
            <div class="img_wrap">
                <div class="col-xs-6 hidden-xs">
                    <div class="row">
                        <img src="dist/img/b2b/Banner1stHalf.jpg" class="img-responsive wow slideInDown" data-wow-delay="2.5s">
                    </div>
                </div>
                <div class="col-xs-6 hidden-xs">
                    <div class="row">
                        <img src="dist/img/b2b/Banner2ndHalf.jpg" class="img-responsive wow slideInUp" data-wow-delay="2.5s">
                    </div>
                </div>
                <div class="col-xs-12 visible-xs">
                    <div class="row">
                        <img src="dist/img/b2b/B2B_Mobile-Banner.jpg" class="img-responsive wow slideInDown" data-wow-delay="2.5s">
                    </div>
                </div>
            </div>
            <div class="b2b-section">
                <img src="dist/img/b2b/b2b-logo.png" class="img-responsive">
            </div>

            <div class="heading b2b-description">
                <h3 class="b2b-subheading">A first in India</h3>
                <p>At Rosentiques, the goal is simple: jewellery like never before. That is why we’ve designed a regal space where you, our valued partners, can experience our creations as luxuriously and comfortably as possible.<br><br> The very first of its kind in the country, the boutique is an exclusive and plush little section of the House of Rosentiques that offers every partner of ours a unique tour of our finest pieces.<br><br>This is jewellery like you’ve never seen or felt it before.<br><br>
                    This is the Rosentiques B2B Boutique.</p>
            </div>

            <!--
            <div class="video-section">
                <img src="dist/img/b2b/video-banner.jpg" class="img-responsive">
                <div class="video-icon">
                    <img src="dist/img/b2b/video-icon.png" class="img-responsive">
                </div>
                
                <div class="video-icon collectionModal" data-src="https://www.youtube.com/watch?v=yRB0xbKDebo">
                    <img src="dist/img/b2b/video-icon.png" class="img-responsive">
                </div>

            </div>
-->

            <div class="heading b2b-description">
                <h3 class="b2b-subheading">Transforming your jewellery experience </h3>
                <p>What makes our B2B boutique a transformational jewellery experience?<br><br> A roomy space. Carpeted floors. A lavish seating area. Aesthetic displays. An air of refinement.<br><br>Our boutique isn’t a boxy room with a corporate desk where you make transactional decisions about the pieces you see.<br><br>It is a place where you take your time to admire the world’s most precious metals and stones.<br><br>Welcome to the Rosentiques B2B Boutique.</p>
            </div>

            <div class="col-xs-12 collection_list">
                <div class="heading">
                    <h2>Our collection</h2>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 padd-lr-10">
                    <div class="items">
                        <img src="dist/img/b2b/b2b-1.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/b2b/b2b-1.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items mar-t-20">
                        <img src="dist/img/b2b/b2b-3.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/b2b/b2b-3.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 no-lr-padd">
                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/b2b/b2b-2.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/b2b/b2b-2.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/b2b/b2b-5.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/b2b/b2b-5.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 padd-lr-10">
                    <div class="items">
                        <img src="dist/img/b2b/b2b-4.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/b2b/b2b-4.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/b2b/b2b-6.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/b2b/b2b-6.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                </div>

            </div>


        </div>

        <!--
        <div class="switch">
            <div class="container">
                <div class="switch-wrapper">
                    <p class="text-center">Switch to</p>

                    <div class="row">
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="amolya.php">Amolya</a></h2>
                            </div>
                        </div>
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="platinum.php">Platinum</a></h2>
                            </div>
                        </div>
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="polki.php">Polki</a></h2>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
-->
    </section>

    <div id="collection_gallery" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <div class="collectionGallery">
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/1.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/2.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/3.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/4.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/5.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/3.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/7.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/8.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/9.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/10.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/11.jpg" class="img-responsive">
                        </div>

                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/12.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/13.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/14.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/15.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/16.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/17.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/18.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/19.jpg" class="img-responsive">
                        </div>

                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/20.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/21.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/22.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/23.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/24.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/25.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/26.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/27.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/28.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/29.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/30.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/31.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/32.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/33.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/34.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/35.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/36.jpg" class="img-responsive">
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

    <script type="text/javascript">
        //modal form 

        var length_value = $(".product_code_details :input").val().length;
        $(".product_code_details :input").focus(function() {

            $(this).parent().find('label').addClass('active');

        }).blur(function() {

            $(this).parent().find('label').removeClass("active");
            if ($(this).val().length > 0) {
                $(this).parent().find('label').addClass("active");
            } else {
                $(this).parent().find('label').removeClass("active");
            }
        });

    </script>


</body>

</html>
