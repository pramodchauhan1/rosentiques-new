<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
    
</head>

<body>
    <style>
        .upcoming_box{
            background-color: #fff;
            padding: 20px 20px 10px;
            overflow: hidden;
            margin-bottom: 30px;
            text-align: center;
            min-height: 346px;
        }
        .upcoming_box p{
            color: #2c161e;
            font-family: "raleway-medium";
        }
        .upcoming_box h3{
            font-size: 20px;
            color: #2c161e;
            font-family: "raleway-medium";
        }
        .logo_img{
            min-height: 167px;
            display: flex;
            align-items: center;
        }
        @media screen and (max-width: 991px) {
          .upcoming-event-section{
              height: auto !important;
          }
        }
        @media screen and (max-width: 1199px) {
            .upcoming_box h3{
                font-size:19px;
            }
        }
    </style>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="upcoming-event" class="upcoming-event main_wrapper">
        <div class="container upcoming-event-section">
            <div class="row">
                <div class="col-md-12 heading">
                    <h2 class="">Upcoming Events</h2>
                </div>
                <div class="col-md-4">
                    <div class="upcoming_box">
                        <div class="logo_img">
                            <img src="dist/img/upcoming/logo.png" alt="" class="img-responsive">
                        </div>
                        <h3>RFJ B2B Show</h3>
                        <p>7th Jan 2022</p>
                        <p>Rosentiques B2B Boutique</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="upcoming_box">
                        <div class="logo_img">
                            <img src="dist/img/upcoming/couture.png" alt="" class="img-responsive">
                        </div>
                        <h3>Couture India Show</h3>
                        <p>26th - 28th February 2022</p>
                        <p>Grand Hyatt, Mumbai</p>
                    </div>
                </div>

                <div class="col-md-4">
                    
                    <div class="upcoming_box">
                        <div class="logo_img">
                            <img src="dist/img/upcoming/gjs.png" alt="" class="img-responsive">
                        </div>
                        <h3>India Gem & Jewellery Show</h3>
                        <p>1st - 4th March 2022</p>
                        <p>Jio World Convention Centre, Mumbai</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>
</body>

</html>
