<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="collections" class="main_wrapper">
        <div class="container">
            <div class="img_wrap">
                <div class="col-xs-6 hidden-xs">
                    <div class="row">
                        <img src="dist/img/Platinum/Banner1stHalf.jpg" class="img-responsive wow slideInDown" data-wow-delay="2.5s">
                    </div>
                </div>
                <div class="col-xs-6 hidden-xs">
                    <div class="row">
                        <img src="dist/img/Platinum/Banner2ndHalf.jpg" class="img-responsive wow slideInUp" data-wow-delay="2.5s">
                    </div>
                </div>
                <div class="col-xs-12 visible-xs">
                    <div class="row">
                        <img src="dist/img/mobile-platinum.jpg" class="img-responsive wow slideInDown" data-wow-delay="2.5s">
                    </div>
                </div>
            </div>

            <div class="desc text-justify">
                <p>
                    The brand stands for the legacy that has been passed down over 4 generations, each one passing down its essence to the next generation which in turn evolves to add its own fervor thereby growing into a unique family matrix. </p>

                <p>Staying true to its name, in its launch year ‘the legacy of Amolya’ begins by celebrating the concept of ’priceless-ness'. Since there is nothing better that could represent 'Amolya' than 'the gift of life' itself 'The legacy of Amolya' kick starts for priceless jewelry collections that represent the 4 elements of nature that make 'life' possible on this beautiful and unique planet Earth. .one of its kind in the whole universe. </p>
            </div>

            <div class="heading">
                <!-- <h2>our collections</h2> -->
                <p>The magical moment when you discover a bond you share is rare. It's a day like no other. Incomparable, beautiful, and unique. From that day, an extraordinary journey begins. And it deserves to be marked by something truly rare and precious: a symbol of your one of a kind love.<br><br>It deserves to be marked by Platinum, by Rosentiques.<br><br>Naturally white, the rarest of precious metals, and the most secure setting for diamonds and precious stones, Platinum is the perfect choice for an expression of love that lasts a lifetime.</p>
            </div>

            <div class="col-xs-12 collection_list">
                <div class="col-md-3 col-sm-3 col-xs-12 padd-lr-10">
                    <div class="items">
                        <img src="dist/img/Platinum/left-1.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/left-1.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items mar-t-20">
                        <img src="dist/img/Platinum/left-2.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/left-2.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items mar-t-20">
                        <img src="dist/img/Platinum/left-3.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/left-3.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items mar-t-20">
                        <img src="dist/img/Platinum/left-4.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/left-4.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="items mar-t-20">
                        <img src="dist/img/Platinum/left-5.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/left-5.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> -->


                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 no-lr-padd">
                    <!-- <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-1.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-1.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-2.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-2.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div> -->

                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-3.png" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-3.png">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-4.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-4.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-5.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-5.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-6.png" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-6.png">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-7.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-7.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-8.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-8.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-9.png" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-9.png">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-10.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-10.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Platinum/center-11.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Platinum/center-11.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div> -->
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12 padd-lr-10">
                    <div class="items">
                        <img src="dist/img/Platinum/right-1.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/right-1.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Platinum/right-2.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/right-2.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Platinum/right-3.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/right-3.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Platinum/right-4.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/right-4.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="items">
                        <img src="dist/img/Platinum/right-5.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/right-5.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> -->
                    <div class="items">
                        <img src="dist/img/Platinum/right-6.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/right-6.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>

                    <div class="items">
                        <img src="dist/img/Platinum/left-5.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Platinum/left-5.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                </div>

            </div>


        </div>

        <div class="switch">
            <div class="container">
                <div class="switch-wrapper">
                    <p class="text-center">Switch to</p>

                    <div class="row">
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="nazaqat.php">Nazaqat</a></h2>
                            </div>
                        </div>
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="amolya.php">Amolya</a></h2>
                            </div>
                        </div>
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="polki.php">Mukhlasi</a></h2>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <div id="collection_gallery" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <div class="collectionGallery">
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/1.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/2.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Platinum/pop-up/3.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/4.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/5.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/6.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/7.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/8.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/9.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/10.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/11.jpg" class="img-responsive">
                        </div>

                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/12.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/13.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/14.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/15.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/16.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/17.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/18.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/19.jpg" class="img-responsive">
                        </div>

                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/20.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/21.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/platinum/pop-up/22.jpg" class="img-responsive">
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

    <script type="text/javascript">
        //modal form 

        var length_value = $(".product_code_details :input").val().length;
        $(".product_code_details :input").focus(function() {

            $(this).parent().find('label').addClass('active');

        }).blur(function() {

            $(this).parent().find('label').removeClass("active");
            if ($(this).val().length > 0) {
                $(this).parent().find('label').addClass("active");
            } else {
                $(this).parent().find('label').removeClass("active");
            }
        });

    </script>


</body>

</html>
