<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
    <style>
        .banner_txt{
            position: relative;
        }
        .txt_position{
            font-family: "strike-your-path";
            color: #d7a156;
            text-transform: capitalize;
            font-size: 130px;
            margin: 0;
            background: linear-gradient(to right,#d59c53 40%,#a43e2d 60%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            position: absolute;
            right: 130px;
            bottom: 100px;
            padding: 0px 42px;
        }
        @media screen and (max-width: 1199px) {
            .txt_position{
                right: 65px;
            }    
        }
        @media screen and (max-width: 991px) {
            .txt_position{
                font-size: 100px;
                right: 45px;
                bottom: 30px;
                padding: 0px 35px;

            }    
        }
        @media screen and (max-width: 767px) {
            .txt_position{
                font-size: 76px;
                right: 0px;
                bottom: 0px;
                padding: 0px 0px;
                text-align: center;
                width: 100%;
                background: linear-gradient(to right,#d59c53 100%,#a43e2d 100%);
                background: linear-gradient(to right,#d59c53 100%,#a43e2d 0%);
                -webkit-text-fill-color: transparent;
                -webkit-background-clip: text;
            }    
        }
    </style>
</head>

<body>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="collections" class="main_wrapper">
        <div class="container">
            <div class="img_wrap">
                <div class="col-xs-12 hidden-xs">
                    <div class="row banner_txt">
                        <img src="dist/img/desktop-nazaqat.jpg" class="img-responsive ">
                        <h2 class="txt_position">Nazaqat</h2>
                    </div>
                </div>
                <!-- <div class="col-xs-6 hidden-xs">
                    <div class="row">
                        <img src="dist/img/Nazaqat/Banner1stHalf.jpg" class="img-responsive wow slideInDown" data-wow-delay="2.5s">
                    </div>
                </div> -->
                <!-- <div class="col-xs-6 hidden-xs">
                    <div class="row">
                        <img src="dist/img/Nazaqat/Banner2ndHalf.jpg" class="img-responsive wow slideInUp" data-wow-delay="2.5s">
                    </div>
                </div> -->
                <div class="col-xs-12 visible-xs">
                    <div class="row banner_txt">
                        <img src="dist/img/mobile-nazaqat.jpg" class="img-responsive " >
                        <h2 class="txt_position">Nazaqat</h2>
                    </div>
                </div>
            </div>

            <div class="desc text-justify">
                <p>
                    The brand stands for the legacy that has been passed down over 4 generations, each one passing down
                    its essence to the next generation which in turn evolves to add its own fervor thereby growing into
                    a unique family matrix. </p>

                <p>Staying true to its name, in its launch year ‘the legacy of Amolya’ begins by celebrating the
                    concept of ’priceless-ness'. Since there is nothing better that could represent 'Amolya' than 'the
                    gift of life' itself 'The legacy of Amolya' kick starts for priceless jewelry collections that
                    represent the 4 elements of nature that make 'life' possible on this beautiful and unique planet
                    Earth. .one of its kind in the whole universe. </p>
            </div>

            <div class="heading ">Nazaqat Jadau Jewellery
                <!-- <h2>our collections</h2> -->
                <p>Presenting Nazaqat Jadau Jewellery: Bikaneri Jadau Jewellery from the house of Rosentiques.<br><br> After
                    successfully establishing ourselves in the category of Mukhlasi, Diamonds and Platinum, we now
                    step into a different segment: 'Traditional Bikaneri Jadau Jewellery'.<br><br>Nazaqat's USP is
                    'impeccable design combined with extremely intricate work, created with the utmost precision'.<br><br>
                    We aim to create valuable products and leverage innovation in order to reach new heights and set
                    bigger goals.<br><br> Rosentiques' Nazaqat is a spectacular newness in Jadau Jewellery to add
                    richness to your life.</p>
            </div>

            <div class="col-xs-12 collection_list">
                <div class="col-md-3 col-sm-3 col-xs-12 padd-lr-10">
                    <div class="items">
                        <img src="dist/img/Nazaqat/left-2.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/left-2.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="items mar-t-20">
                        <img src="dist/img/Nazaqat/left-1.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/left-1.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> -->
                    <div class="items mar-t-20">
                        <img src="dist/img/Nazaqat/left-3.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/left-3.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items mar-t-20">
                        <img src="dist/img/Nazaqat/left-4.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/left-4.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>

                    <!-- <div class="items">
                        <img src="dist/img/Nazaqat/left-5.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/left-5.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items mar-t-20">
                        <img src="dist/img/Nazaqat/left-6.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/left-6.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> -->

                    <!-- <div class="items mar-t-20">
    <img src="dist/img/Nazaqat/left-7.jpg" class="img-responsive">
    <div class="overlay">
        <a class="collectionModal" data-src="dist/img/Nazaqat/left-7.jpg">
            <img src="dist/img/resize.png">
        </a>
    </div>
</div>
<div class="items mar-t-20">
    <img src="dist/img/Nazaqat/left-8.jpg" class="img-responsive">
    <div class="overlay">
        <a class="collectionModal" data-src="dist/img/Nazaqat/left-8.jpg">
            <img src="dist/img/resize.png">
        </a>
    </div>
</div> -->
                    <!-- <div class="items mar-t-20">
                        <img src="dist/img/Nazaqat/left-9.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/left-9.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> -->

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 no-lr-padd">
                    <!-- <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-1.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-1.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div> -->

                    <!-- <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-2.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-2.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div> 

                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-3.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-3.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>-->

                    <!--
                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-4.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-4.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
-->

                    <!--
                    <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-5.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-5.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
-->



                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-6.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-6.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-9.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-9.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
-->

                    <!-- <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-10.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-10.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div> -->

                    <!-- <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-11.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-11.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div> -->

                    <!--
                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-12.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-12.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
-->
                    <!-- <div class="col-sm-6 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-15.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-15.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div> -->

                    <div class="col-sm-12 col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-14.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-14.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-13.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-13.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-16.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-16.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-xs-12 padd-lr-10">
                        <div class="items">
                            <img src="dist/img/Nazaqat/center-17.jpg" class="img-responsive">
                            <div class="overlay">
                                <a class="collectionModal" data-src="dist/img/Nazaqat/center-17.jpg">
                                    <img src="dist/img/resize.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12 padd-lr-10">
                    <div class="items">
                        <img src="dist/img/Nazaqat/right-1.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/right-1.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Nazaqat/right-2.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/right-2.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="items">
                        <img src="dist/img/Nazaqat/right-3.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/right-3.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> 
                    <div class="items">
                        <img src="dist/img/Nazaqat/right-4.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/right-4.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Nazaqat/right-5.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/right-5.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Nazaqat/right-6.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/right-6.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Nazaqat/right-7.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/right-7.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <img src="dist/img/Nazaqat/right-8.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/right-8.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> -->

                    <div class="items">
                        <img src="dist/img/Nazaqat/right-9.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/right-9.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="items">
                        <img src="dist/img/Nazaqat/center-2.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/center-2.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> -->
                    <!-- <div class="items">
                        <img src="dist/img/Nazaqat/center-11.jpg" class="img-responsive">
                        <div class="overlay">
                            <a class="collectionModal" data-src="dist/img/Nazaqat/center-11.jpg">
                                <img src="dist/img/resize.png">
                            </a>
                        </div>
                    </div> -->
                    <!-- <div class="items">
    <img src="dist/img/Nazaqat/right-10.jpg" class="img-responsive">
    <div class="overlay">
        <a class="collectionModal" data-src="dist/img/Nazaqat/right-10.jpg">
            <img src="dist/img/resize.png">
        </a>
    </div>
</div>
<div class="items">
    <img src="dist/img/Nazaqat/right-11.jpg" class="img-responsive">
    <div class="overlay">
        <a class="collectionModal" data-src="dist/img/Nazaqat/right-11.jpg">
            <img src="dist/img/resize.png">
        </a>
    </div>
</div> -->

                </div>

            </div>


        </div>

        <div class="switch">
            <div class="container">
                <div class="switch-wrapper">
                    <p class="text-center">Switch to</p>

                    <div class="row">
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="amolya.php">Amolya</a></h2>
                            </div>
                        </div>
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="platinum.php">Platinum</a></h2>
                            </div>
                        </div>
                        <div class="col-sm-4 common-wrapper">
                            <div class="switch-content-wrapper">
                                <h2><a href="polki.php">Mukhlasi</a></h2>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <div id="collection_gallery" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <div class="collectionGallery">
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/1.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/2.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/3.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/4.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/5.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/3.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/7.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/8.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/9.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/10.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/11.jpg" class="img-responsive">
                        </div>

                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/12.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/13.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/14.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/15.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/16.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/17.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/18.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/19.jpg" class="img-responsive">
                        </div>

                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/20.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/21.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/22.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/23.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/24.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/25.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/26.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/27.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/28.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/29.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/30.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/31.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/32.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/33.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/34.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/35.jpg" class="img-responsive">
                        </div>
                        <div class="item slick-slide">
                            <img src="dist/img/Nazaqat/pop-up/36.jpg" class="img-responsive">
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

    <script type="text/javascript">
        //modal form 

        var length_value = $(".product_code_details :input").val().length;
        $(".product_code_details :input").focus(function() {

            $(this).parent().find('label').addClass('active');

        }).blur(function() {

            $(this).parent().find('label').removeClass("active");
            if ($(this).val().length > 0) {
                $(this).parent().find('label').addClass("active");
            } else {
                $(this).parent().find('label').removeClass("active");
            }
        });

    </script>


</body>

</html>
