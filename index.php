<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>
<style>
    .banner_txt{
        position: relative;
    }
    .txt_position{
        font-family: "strike-your-path";
        color: #d7a156;
        text-transform: capitalize;
        font-size: 160px;
        margin: 0;
        background: linear-gradient(to right,#d59c53 40%,#a43e2d 60%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        position: absolute;
        right: 30px;
        bottom: 50px;
    }
    @media screen and (max-width: 1199px) {
        .txt_position{
            font-size: 95px;
        }    
    }
    @media screen and (max-width: 991px) {
        .txt_position{
            font-size: 60px;
            bottom: 25px;
        }    
    }
    @media screen and (max-width: 767px) {
        .txt_position{
            font-size: 58px;
            right: -50%;
            bottom: 0px;
            text-align: center;
            transform: translateX(-50%);
            width: 100%;
            background: linear-gradient(to right,#d59c53 100%,#a43e2d 100%);
            background: linear-gradient(to right,#d59c53 100%,#a43e2d 0%);
            -webkit-text-fill-color: transparent;
            -webkit-background-clip: text;
        }    
    }
</style>

<body class="frontPage_body" onload="slideBanner()">
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="banner">
        <div id="animation_container">
            <canvas id="canvas" width="250" height="130" style="position: absolute; display: block;"></canvas>
            <div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:250px; height:130px; position: absolute; left: 0px; top: 0px; display: block;">
            </div>
        </div>
        <div class="banner_section1 wow slideInDown" data-wow-delay="0.5s" data-wow-duration="1s">
            <!-- <img src="dist/img/banner3.png" class="img-responsive"> -->
        </div>
        <div class="banner_section2 wow slideInUp" data-wow-delay="0.5s" data-wow-duration="1s">
            <!-- <img src="dist/img/banner2.png" class="img-responsive"> -->
        </div>
        <a class="text-center scroll_down">
            <img src="dist/img/scroll.png">
        </a>
    </section>

    <section id="slider_wrapper">
        <a href="index.php" class="frontpage_logo">
            <img src="dist/img/logo.png">
        </a>
        <div class="split-slideshow">
            <div class="slideshow">
                <div class="slider">
                    <div class="item">
                        <a href="polki.php"><img src="dist/img/desktop-polki.jpg" class="img-responsive show_desktop_portrait" /></a>
                        <a href="polki.php"><img src="dist/img/mobile-polki.jpg" class="img-responsive show_mobile_portrait" /></a>
                        
                    </div>
                    <div class="item">
                        <a href="amolya.php"><img src="dist/img/desktop-amolya.jpg" class="img-responsive show_desktop_portrait" /></a>
                        <a href="amolya.php"><img src="dist/img/mobile-amolya.jpg" class="img-responsive show_mobile_portrait" /></a>
                    </div>
                    <div class="item">
                        <!-- <div class="banner_txt">
                            
                            <h2 class="txt_position">Nazaqat <br>Jadau Jewellery</h2>
                        </div> -->
                            <a href="nazaqat.php">
                                <img src="dist/img/desktop-nazaqat.jpg" class="img-responsive show_desktop_portrait" />
                            </a>
                        
                            <a href="nazaqat.php"><img src="dist/img/mobile-nazaqat.jpg" class="img-responsive show_mobile_portrait" /></a>
                            <h2 class="txt_position">Nazaqat <br class="hidden-xs">Jadau Jewellery</h2>
                    </div>

                    <div class="item">
                        <a href="platinum.php"><img src="dist/img/Platinum/Banner_2.jpg" class="img-responsive show_desktop_portrait" /></a>
                        <a href="platinum.php"><img src="dist/img/mobile-platinum.jpg" class="img-responsive show_mobile_portrait" /></a>
                    </div>

                    <!-- <div class="item">
                        <a href="javascript:void(0);"><img src="dist/img/desktop-rosa-amoris.jpg" class="img-responsive show_desktop_portrait" /></a>
                        <a href="javascript:void(0);"><img src="dist/img/mobile-rosa-amoris.jpg" class="img-responsive show_mobile_portrait" /></a>
                    </div>

                    <div class="item">
                        <a href="javascript:void(0);"><img src="dist/img/desktop-rosentiques-all.jpg" class="img-responsive show_desktop_portrait" /></a>
                        <a href="javascript:void(0);"><img src="dist/img/mobile-rosentiques-all.jpg" class="img-responsive show_mobile_portrait" /></a>
                    </div> -->

                </div>
            </div>

        </div>

        <div class="slider_details">

            <div class="text-right slideshow-text">
                <a href="amolya.php" class="know_more"> <span>Know More </span> <img src="dist/img/right-arrow.png"></a>
                <a href="nazaqat.php" class="know_more"> <span>Know More </span> <img src="dist/img/right-arrow.png"></a>
                <a href="polki.php" class="know_more"> <span>Know More </span> <img src="dist/img/right-arrow.png"></a>
                <a href="platinum.php" class="know_more"> <span>Know More </span> <img src="dist/img/right-arrow.png"></a>
                <a href="javascript:void(0);" class="know_more"> <span>Know More </span> <img src="dist/img/right-arrow.png"></a>
                <a href="javascript:void(0);" class="know_more"> <span>Know More </span> <img src="dist/img/right-arrow.png"></a>
            </div>

            <div class="slideshow-number">
                <div class="item">01</div>
                <div class="item">02</div>
                <div class="item">03</div>
                <div class="item">04</div>
                <div class="item">05</div>
                <div class="item">06</div>
            </div>

            <button class="next_btn">next</button>
        </div>
    </section>


    <?php //include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>
    <script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
    <script src="canvas/canvas.js"></script>
    <script>
        var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

        function init() {
            canvas = document.getElementById("canvas");
            anim_container = document.getElementById("animation_container");
            dom_overlay_container = document.getElementById("dom_overlay_container");
            var comp = AdobeAn.getComposition("15309FA4602A4149B0EB81E55DDE474C");
            var lib = comp.getLibrary();
            var loader = new createjs.LoadQueue(false);
            loader.addEventListener("fileload", function(evt) {
                handleFileLoad(evt, comp)
            });
            loader.addEventListener("complete", function(evt) {
                handleComplete(evt, comp)
            });
            var lib = comp.getLibrary();
            loader.loadManifest(lib.properties.manifest);
        }

        function handleFileLoad(evt, comp) {
            var images = comp.getImages();
            if (evt && (evt.item.type == "image")) {
                images[evt.item.id] = evt.result;
            }
        }

        function handleComplete(evt, comp) {
            //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
            var lib = comp.getLibrary();
            var ss = comp.getSpriteSheet();
            var queue = evt.target;
            var ssMetadata = lib.ssMetadata;
            for (i = 0; i < ssMetadata.length; i++) {
                ss[ssMetadata[i].name] = new createjs.SpriteSheet({
                    "images": [queue.getResult(ssMetadata[i].name)],
                    "frames": ssMetadata[i].frames
                })
            }
            exportRoot = new lib.ROSENTIQUES_CANVAS();
            stage = new lib.Stage(canvas);
            //Registers the "tick" event listener.
            fnStartAnimation = function() {
                stage.addChild(exportRoot);
                createjs.Ticker.setFPS(lib.properties.fps);
                createjs.Ticker.addEventListener("tick", stage)
                stage.addEventListener("tick", handleTick)

                function getProjectionMatrix(container, totalDepth) {
                    var focalLength = 528.25;
                    var projectionCenter = {
                        x: lib.properties.width / 2,
                        y: lib.properties.height / 2
                    };
                    var scale = (totalDepth + focalLength) / focalLength;
                    var scaleMat = new createjs.Matrix2D;
                    scaleMat.a = 1 / scale;
                    scaleMat.d = 1 / scale;
                    var projMat = new createjs.Matrix2D;
                    projMat.tx = -projectionCenter.x;
                    projMat.ty = -projectionCenter.y;
                    projMat = projMat.prependMatrix(scaleMat);
                    projMat.tx += projectionCenter.x;
                    projMat.ty += projectionCenter.y;
                    return projMat;
                }

                function handleTick(event) {
                    var cameraInstance = exportRoot.___camera___instance;
                    if (cameraInstance !== undefined && cameraInstance.pinToObject !== undefined) {
                        cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                        cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                        if (cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                    }
                    applyLayerZDepth(exportRoot);
                }

                function applyLayerZDepth(parent) {
                    var cameraInstance = parent.___camera___instance;
                    var focalLength = 528.25;
                    var projectionCenter = {
                        'x': 0,
                        'y': 0
                    };
                    if (parent === exportRoot) {
                        var stageCenter = {
                            'x': lib.properties.width / 2,
                            'y': lib.properties.height / 2
                        };
                        projectionCenter.x = stageCenter.x;
                        projectionCenter.y = stageCenter.y;
                    }
                    for (child in parent.children) {
                        var layerObj = parent.children[child];
                        if (layerObj == cameraInstance)
                            continue;
                        applyLayerZDepth(layerObj, cameraInstance);
                        if (layerObj.layerDepth === undefined)
                            continue;
                        if (layerObj.currentFrame != layerObj.parent.currentFrame) {
                            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                        }
                        var matToApply = new createjs.Matrix2D;
                        var cameraMat = new createjs.Matrix2D;
                        var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                        var cameraDepth = 0;
                        if (cameraInstance && !layerObj.isAttachedToCamera) {
                            var mat = cameraInstance.getMatrix();
                            mat.tx -= projectionCenter.x;
                            mat.ty -= projectionCenter.y;
                            cameraMat = mat.invert();
                            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                            if (cameraInstance.depth)
                                cameraDepth = cameraInstance.depth;
                        }
                        if (layerObj.depth) {
                            totalDepth = layerObj.depth;
                        }
                        //Offset by camera depth
                        totalDepth -= cameraDepth;
                        if (totalDepth < -focalLength) {
                            matToApply.a = 0;
                            matToApply.d = 0;
                        } else {
                            if (layerObj.layerDepth) {
                                var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                                if (sizeLockedMat) {
                                    sizeLockedMat.invert();
                                    matToApply.prependMatrix(sizeLockedMat);
                                }
                            }
                            matToApply.prependMatrix(cameraMat);
                            var projMat = getProjectionMatrix(parent, totalDepth);
                            if (projMat) {
                                matToApply.prependMatrix(projMat);
                            }
                        }
                        layerObj.transformMatrix = matToApply;
                    }
                }
            }
            //Code to support hidpi screens and responsive scaling.
            function makeResponsive(isResp, respDim, isScale, scaleType) {
                var lastW, lastH, lastS = 1;
                window.addEventListener('resize', resizeCanvas);
                resizeCanvas();

                function resizeCanvas() {
                    var w = lib.properties.width,
                        h = lib.properties.height;
                    var iw = window.innerWidth,
                        ih = window.innerHeight;
                    var pRatio = window.devicePixelRatio || 1,
                        xRatio = iw / w,
                        yRatio = ih / h,
                        sRatio = 1;
                    if (isResp) {
                        if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
                            sRatio = lastS;
                        } else if (!isScale) {
                            if (iw < w || ih < h)
                                sRatio = Math.min(xRatio, yRatio);
                        } else if (scaleType == 1) {
                            sRatio = Math.min(xRatio, yRatio);
                        } else if (scaleType == 2) {
                            sRatio = Math.max(xRatio, yRatio);
                        }
                    }
                    canvas.width = w * pRatio * sRatio;
                    canvas.height = h * pRatio * sRatio;
                    canvas.style.width = dom_overlay_container.style.width = anim_container.style.width = w * sRatio + 'px';
                    canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h * sRatio + 'px';
                    stage.scaleX = pRatio * sRatio;
                    stage.scaleY = pRatio * sRatio;
                    lastW = iw;
                    lastH = ih;
                    lastS = sRatio;
                    stage.tickOnUpdate = false;
                    stage.update();
                    stage.tickOnUpdate = true;
                }
            }
            makeResponsive(true, 'both', false, 1);
            AdobeAn.compositionLoaded(lib.properties.id);
            fnStartAnimation();
        }


        $(window).on('load', function() {
            setTimeout(function() {
                $('.loader_overlay').hide();
                init();
            }, 1200);
        });

    </script>



    <!-- <script>
	  // new Vivus('my-div', {duration: 200, file: 'link/to/my.svg'}, myCallback);
	  new Vivus('svg', {duration: 400, file: 'dist/img/Logo_Rosentiques_1-01.svg', type: 'oneByOne'});
	</script> -->



</body>

</html>
