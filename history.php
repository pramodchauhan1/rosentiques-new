<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
        <!--loader-->
        <div class="page-loader">
            <img src="dist/img/page-loader.gif" class="img-responsive loader">
        </div>
        <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="history" class="main_wrapper">
        <div class="container">
            <div class="heading text-center">
                <h2>history <img class="bangle" src="dist/img/bangle.png"> </h2>

            </div>

            <div class="col-xs-12 padd-t-100">
                <div class="row history_desc wow fadeInUp" data-wow-delay="0s" data-wow-duration="1.5s">
                    <div class="col-sm-5 col-md-5 col-lg-4 col-xs-12">
                        <img src="dist/img/history_img1.png" class="img-responsive">
                    </div>
                    <div class="col-sm-7 col-xs-12">
                        <div class="desc">
                            <p>It all started long back when Mr.Amichand Nahar and his son Amlockchand Nahar travelled far and wide from their base in the Nawabi city of Lucknow to supply jewellery to the Royal Families.
                            </p>

                            <p>Their farsight and sheer determination resulted in opening a store in Lucknow in mid-nineties. As Mr. Amlokchand Nahar was joined by his son Mr. Abhaykumar Nahar in the second half of 1900, they made the brave move to migrate to Mumbai. And this was just the beginning. Over the enough years, Struggle, Hard work up and downs followed. But the journey had just began.</p>
                        </div>
                    </div>
                    <div class="col-md-1 hidden-xs hidden-sm"></div>
                </div>

                <div class="row history_desc wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1.5s">
                    <div class="col-md-1 hidden-xs hidden-sm"></div>
                    <div class="col-sm-5 col-md-5 col-lg-4 col-xs-12 pull-right">
                        <img src="dist/img/history_img2.png" class="img-responsive">
                    </div>
                    <div class="col-sm-7 col-xs-12">
                        <div class="desc odd">
                            <p>It all started long back when Mr.Amichand Nahar and his son Amlockchand Nahar travelled far and wide from their base in the Nawabi city of Lucknow to supply jewellery to the Royal Families.
                            </p>

                            <p>Their farsight and sheer determination resulted in opening a store in Lucknow in mid-nineties. As Mr. Amlokchand Nahar was joined by his son Mr. Abhaykumar Nahar in the second half of 1900, they made the brave move to migrate to Mumbai. And this was just the beginning. Over the enough years, Struggle, Hard work up and downs followed. But the journey had just began.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>



    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>


</body>

</html>
