<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="jewellery-care" class="main_wrapper">
        <div class="container">
            <div class="heading">
                <h2>Jewellery Care</h2>
                <h6>Because everything we Love needs Care</h6>
                <p>Not many know that the word “Diamond’ comes from the Greek word ‘Adamas’ and this means
                    ‘unconquerable and indestructible.’ Another lesser known fact is that 80% of the world’s diamonds
                    are not suitable for jewellery therefore the rest 20% deserve gratitude and extra care.<br>Avoid
                    wearing diamonds while doing housework or any kind of rough work. Even though a diamond is
                    extremely durable, a hard blow could chip it. Also when doing household chores, never allow diamond
                    jewellery to come into contact with cholrine bleach.</p>
            </div>
            <div class="jewellery-content">
                <div class="row">
                    <div class="col-md-7 col-sm-12 no-padding">
                        <div class="jewellery-content-image"><img src="dist/img/jewellery-care/jewellery.jpg" class="img-responsive"></div>
                    </div>
                    <div class="col-md-5 col-sm-12 no-padding">
                        <div class="jewellery-desc">
                            <div class="jewellery-desc-content">
                                <h5>Mukhlasi/Nazaqat Jewellery</h5>
                                <p>Mukhlasi/Nazaqat Jewellery must always be kept away from moisture, especially in a city like Mumbai, where it tends to oxidize and tarnish the jewellery. One should never apply perfume or hair spray directly on the jewellery.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 no-padding hidden-md hidden-lg">
                        <div class="jewellery-content-image"><img src="dist/img/jewellery-care/diamonds.jpg" class="img-responsive"></div>
                    </div>
                    <div class="col-md-5 col-sm-12 no-padding">
                        <div class="jewellery-desc">
                            <div class="jewellery-desc-content odd">
                                <h5>Diamonds</h5>
                                <p>It is a very bad practise to use pins or brushes to clean the diamond jewellery.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12 no-padding hidden-xs hidden-sm">
                        <div class="jewellery-content-image"><img src="dist/img/jewellery-care/diamonds.jpg" class="img-responsive"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 no-padding">
                        <div class="jewellery-content-image"><img src="dist/img/jewellery-care/pearls.jpg" class="img-responsive"></div>
                    </div>
                    <div class="col-md-5 no-padding">
                        <div class="jewellery-desc">
                            <div class="jewellery-desc-content">
                                <h5>Pearls</h5>
                                <p>Pearls and color stone jewellery must never be kept loosely with diamonds, because
                                    the diamonds will leave scratches on them.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 no-padding hidden-md hidden-lg">
                        <div class="jewellery-content-image"><img src="dist/img/jewellery-care/earrings.jpg" class="img-responsive"></div>
                    </div>
                    <div class="col-md-5 no-padding">
                        <div class="jewellery-desc">
                            <div class="jewellery-desc-content odd">
                                <h5>Earrings</h5>
                                <p>Ideally, even if 2 pieces of the same pair of earrings are kept in a pouch, they
                                    should be packed individually in separate zip-lock bags.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 no-padding hidden-xs hidden-sm">
                        <div class="jewellery-content-image"><img src="dist/img/jewellery-care/earrings.jpg" class="img-responsive"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

</body>

</html>
