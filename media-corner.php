<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="media_corner" class="main_wrapper">
        <div class="container">
            <div class="heading text-center">
                <h2>media corner <img class="ring" src="dist/img/ring.png"></h2>

            </div>

            <div class="row">
                <div class="media_gallery">
                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item" data-toggle="modal" data-target="#gallery" data-slide="1">
                            <img src="dist/img/media-c1.png" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item" data-toggle="modal" data-target="#gallery" data-slide="2">
                            <img src="dist/img/media-c2.png" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item">
                            <img src="dist/img/media-c3.png" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item">
                            <img src="dist/img/media-c4.png" class="img-responsive" />
                        </a>
                    </div>

                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item" data-toggle="modal" data-target="#gallery" data-slide="1">
                            <img src="dist/img/media-c1.png" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item" data-toggle="modal" data-target="#gallery" data-slide="2">
                            <img src="dist/img/media-c2.png" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item">
                            <img src="dist/img/media-c3.png" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item">
                            <img src="dist/img/media-c4.png" class="img-responsive" />
                        </a>
                    </div>


                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item" data-toggle="modal" data-target="#gallery" data-slide="1">
                            <img src="dist/img/media-c1.png" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item" data-toggle="modal" data-target="#gallery" data-slide="2">
                            <img src="dist/img/media-c2.png" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item">
                            <img src="dist/img/media-c3.png" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-sm-6 col-xs-12 col-md-3">
                        <a class="media_item">
                            <img src="dist/img/media-c4.png" class="img-responsive" />
                        </a>
                    </div>

                </div>

                <img class="sticky_img" src="dist/img/earings.png">
            </div>

        </div>
    </section>

    <div id="gallery" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <div class="mediaCorner">
                        <div class="item slick-slide">
                            <div class="col-sm-7 col-xs-12 media_img">
                                <div class="row">
                                    <img src="dist/img/media-modal-c1.png" class="img-responsive">
                                </div>
                            </div>
                            <div class="col-sm-5 col-xs-12 media_text">
                                <div class="row">
                                    <div class="media_content text-center">
                                        <img src="dist/img/modal-design.png">

                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                                        <img class="logo" src="dist/img/logo.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item slick-slide">
                            <div class="col-sm-7 col-xs-12 media_img">
                                <div class="row">
                                    <img src="dist/img/media-modal-c2.png" class="img-responsive">
                                </div>
                            </div>
                            <div class="col-sm-5 col-xs-12 media_text">
                                <div class="row">
                                    <div class="media_content text-center">
                                        <img src="dist/img/modal-design.png">

                                        <div class="desc">
                                            <p>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book
                                            </p>
                                        </div>

                                        <img class="logo" src="dist/img/logo.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    </div>



    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>



</body>

</html>
