<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>

    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="milestone" class="main_wrapper">
        <div class="container">
            <div class="heading text-center">
                <h2>journey
                    <!-- <img class="ring" src="dist/img/ring-milestone.png"> -->
                </h2>

            </div>
            <div class="desc text-justify">
                <p>The journey from beginning as an idea and growing into a full-fledged company is long and eventful. This is the Rosentiques journey: one that spans four generations.</p>
            </div>

            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="main-timeline">

                    <div class="timeline">
                        <div class="timeline-content" style="margin-top:50px">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step1.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-even">1892</h3>
                                    <p>The great and well-remembered Amirchandji makes his way from Rajasthan to Lucknow: the city where Rosentiques’ foundations would be laid. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step2.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-odd">1917</h3>
                                    <p>Amirchandji decides to start his own gems and jewellery business. His decision would mark the starting point of a new period in Rosentiques’ history.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step3.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-even">1946</h3>
                                    <p>Once again, a new chapter in the story of this enterprise: Amolakhchandji, Amirchandji’s son, joins his father's business. This would be a crucial phase for the company.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step4.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-odd">1953 </h3>
                                    <p>New horizons are explored by the company as Amolakhchandji starts doing business with royalty and even begins dealing in rare jewels.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step5.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-even">1970 </h3>
                                    <p>Amolakhchandji makes his mark in a future home of Rosentiques. He launches his very own retail venture in the City of Nawabs.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step6.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-odd">1973</h3>
                                    <p>The next generation takes over. Raja Nahar, Amolakhchandji’s son, inherits the business from his father; another huge beginning for the company.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step7.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-even">1977 </h3>
                                    <p> The enterprise makes its way to India’s commercial capital, Mumbai. This would mark not just a change in location, but also the dawn of a whole new Rosentiques era.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step8.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-odd">1985 </h3>
                                    <p>Raja Nahar starts exploring the possibilities of exports for his enterprise: a business move that would pave the way for untold successes.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <!-- <h3 class="date-even">ROSENTIQUES</h3> -->
                                <img src="dist/img/milstone/step9.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-even">1996 </h3>
                                    <p> A momentous year for the company: the now celebrated name ‘Rosentiques’ is coined, and the first ‘Rosentiques’ showroom is launched and operated by Raja Nahar's wife Pratibha Nahar.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step10.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-odd">2002 </h3>
                                    <p> After studies at the GIA, Carlsbad, California and a Master’s in Finance in London, Siddharth Nahar joins his father’s business.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/2003.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-even">2003 </h3>
                                    <p>Shortly after, Adeesh, Siddharth’s brother joins the reigns, and another new era begins for the reputed jewellery company.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/2005.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-odd">2005 </h3>
                                    <p>A new and crucial frontier: Rosentiques expands into the B2B space, a milestone reflecting major business progress and a whole new adventure for Team Rosentiques.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step13.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-even">2012 </h3>
                                    <p>Another major milestone: the business gets a marketing division in Kemp’s Corner, Mumbai: an upscale neighbourhood with blooming trade possibilities.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step14.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-odd">2014</h3>
                                    <p>Rosentiques gets associated with <a class="journey-link" target="_blank" href="platinum.php">Platinum</a>, and the company becomes the country’s first manufacturer of multiple kinds of jewels.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step15.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-even">2019</h3>
                                    <p>India’s very first B2B Jewellery Experience Boutique, by Rosentiques, opens at Kemp’s Corner: an outlet that’s sure to be the first of numerous in the Maximum City.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <div class="timeline-icon">
                                <img src="dist/img/milstone/step16.jpg" class="img-responsive">
                            </div>
                            <div class="content">
                                <div class="details">
                                    <h3 class="date-odd">2020</h3>
                                    <p>Mukhlasi Polki Jewellery, Amolya Diamond Jewellery, Nazaqat Jadau Jewellery and Rosa Amoris Rosecut Jewellery</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-center">To be continued ... </p>
            </div>
        </div>
        <!--        <img src="dist/img/milestone-bangle.png" class="bangle">-->
    </section>



    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>


</body>

</html>
