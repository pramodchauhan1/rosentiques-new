<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="overview" class="main_wrapper">
        <div class="container">
            <div class="heading text-center">
                <h2>overview </h2>
            </div>

            <div class="col-sm-12 col-md-10 col-md-offset-1">

                <div class="content">
                    <div class="item col-xs-12">
                        <h3>Rosentiques at a Glance</h3>
                        <div class="desc">
                            <p>The House of Rosentiques is in the elite and artistic business of designing and manufacturing fine jewellery. True to the acclaimed name behind the leading brands like Amolya Diamond Jewellery, Nazaqat Jadau Jewellery, Mukhlasi Polki Jewellery and Rosa Amoris Rosecut Jewellery, we combine international standards with Indian values in every ornament we design. Home to niche jewellery that is formed innovatively, Rosentiques glories in a creative process that is backed by the latest market research. This is why we succeed in creating revolutionary, timeless jewellery consistently.

</p>
                        </div>
                        <img src="dist/img/overview1.jpg" class="img-responsive" alt="overview 1">
                    </div>

                    <div class="item col-xs-12">
                        <h3>A Royal Mission, Vision, and Value System</h3>
                        <div class="desc">
                            <p>The Rosentiques mission is simple: to delight our customers with creative products and superior services.</p>
                        </div>
                        <!--                      <img src="dist/img/overview2.jpg" class="img-responsive" alt="overview 1">-->
                        <div class="desc">
                            <!--                            <div class="sub_head">You are the inspiration behind our finest creations.</div>-->
                            <p>You are the inspiration behind our finest creations. We cherish the vision of being the trendsetters in polki and diamond-studded jewellery manufacturing, by consistently pursuing high standards while pioneering creative and innovative designs. Finally, Rosentiques stands on a firm value system that:</p>
                            <ul class="custom-circle">
                                <li>Includes transparency in practices and processes</li>
                                <li>A deep focus on strengthening partnerships</li>
                                <li>Embracing new opportunities</li>
                                <li>An ethical approach to business</li>
                                <li>Emphasis on creativity and innovation, and</li>
                                <li>Conflict-free sourcing that stems from our client-centric outlook</li>
                            </ul>
                            <p class="margin-top-custom-desc">We believe strong core values create the ideal base for an even stronger business standing.</p>
                        </div>
                    </div>

                    <div class="item col-xs-12">
                        <h3>A Rich Legacy of Fine Jewellery </h3>
                        <div class="desc">
                            <p>For decades, Rosentiques has been a jewellery house well known for its passion for innovative and exclusive designs, zest for good craftsmanship, zeal for detailing with precious gems, other fine touches and constant attention to quality. Our skill and artistry are backed by an in-depth understanding of the market and highly responsive sales and after-sales service set-ups. These features combined with a business model that places you as a customer at our centre have made us a celebrated name in the global jewellery space.</p>
                        </div>
                        <img src="dist/img/overview3.jpg" class="img-responsive" alt="overview 1">

                    </div>

                </div>

            </div>
        </div>
        <!--        <img src="dist/img/overview_diamond.png" class="diamond">-->
    </section>



    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>


</body>

</html>
