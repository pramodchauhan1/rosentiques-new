<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

    <section id="edge" class="main_wrapper">
        <div class="container">
            <div class="heading text-center">
                <h2>rosentiques edge</h2>
            </div>

            <div class="col-xs-12 padd-t-100">

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="dist/img/february-2020.pdf" target="_blank">
                        <div class="news_wrap">
                            <img src="dist/img/rosentiques-edge-7.jpg" alt="newsjan2020" class="img-responsive news">
                            <div class="date-download">
                                <h3>Issue 7</h3>
                                <div class="col-xs-12 padd-lr-no">
                                    <div class="col-xs-12 col-sm-12 padd-lr-no text-left">
                                        <img src="dist/img/calender.png">
                                        <span>February 2020</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="dist/img/october19.pdf" target="_blank">
                        <div class="news_wrap">
                            <img src="dist/img/newsoct.jpg" alt="newsoct" class="img-responsive news">
                            <div class="date-download">
                                <h3>Issue 6</h3>
                                <div class="col-xs-12 padd-lr-no">
                                    <div class="col-xs-12 col-sm-12 padd-lr-no text-left">
                                        <img src="dist/img/calender.png">
                                        <span>October 2019</span>
                                    </div>
                                    <!-- <div class="col-xs-6 col-sm-6 padd-lr-no text-right">
                                    <a href="dist/img/jan.pdf" target="_blank" download><img src="dist/img/download.png"></a>
                                </div> -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="dist/img/may19.pdf" target="_blank">
                        <div class="news_wrap">
                            <img src="dist/img/newsmay.jpg" alt="newsmay" class="img-responsive news">
                            <div class="date-download">
                                <h3>Issue 5</h3>
                                <div class="col-xs-12 padd-lr-no">
                                    <div class="col-xs-12 col-sm-12 padd-lr-no text-left">
                                        <img src="dist/img/calender.png">
                                        <span>May 2019</span>
                                    </div>
                                    <!-- <div class="col-xs-6 col-sm-6 padd-lr-no text-right">
                                    <a href="dist/img/jan.pdf" target="_blank" download><img src="dist/img/download.png"></a>
                                </div> -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="dist/img/february.pdf" target="_blank">
                        <div class="news_wrap">
                            <img src="dist/img/newsfeb.jpg" alt="newsfeb" class="img-responsive news">
                            <div class="date-download">
                                <h3>Issue 4</h3>
                                <div class="col-xs-12 padd-lr-no">
                                    <div class="col-xs-12 col-sm-12 padd-lr-no text-left">
                                        <img src="dist/img/calender.png">
                                        <span>February 2019</span>
                                    </div>
                                    <!-- <div class="col-xs-6 col-sm-6 padd-lr-no text-right">
                                    <a href="dist/img/jan.pdf" target="_blank" download><img src="dist/img/download.png"></a>
                                </div> -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="dist/img/august.pdf" target="_blank">
                        <div class="news_wrap">
                            <img src="dist/img/news2.jpg" alt="news1" class="img-responsive news">
                            <div class="date-download">
                                <h3>Issue 3</h3>
                                <div class="col-xs-12 padd-lr-no">
                                    <div class="col-xs-12 col-sm-12 padd-lr-no text-left">
                                        <img src="dist/img/calender.png">
                                        <span>August 2018</span>
                                    </div>
                                    <!-- <div class="col-xs-6 col-sm-6 padd-lr-no text-right">
                                    <a href="dist/img/aug.pdf" target="_blank" download><img src="dist/img/download.png"></a>
                                </div> -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="dist/img/may.pdf" target="_blank">
                        <div class="news_wrap">
                            <img src="dist/img/news1.jpg" alt="news1" class="img-responsive news">
                            <div class="date-download">
                                <h3>Issue 2</h3>
                                <div class="col-xs-12 padd-lr-no">
                                    <div class="col-xs-12 col-sm-12 padd-lr-no text-left">
                                        <img src="dist/img/calender.png">
                                        <span>May 2018</span>
                                    </div>
                                    <!-- <div class="col-xs-6 col-sm-6 padd-lr-no text-right">
                                    <a href="dist/img/may.pdf" target="_blank" download><img src="dist/img/download.png"></a>
                                </div> -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="dist/img/january.pdf" target="_blank">
                        <div class="news_wrap">
                            <img src="dist/img/news.jpg" alt="news1" class="img-responsive news">
                            <div class="date-download">
                                <h3>Issue 1</h3>
                                <div class="col-xs-12 padd-lr-no">
                                    <div class="col-xs-12 col-sm-12 padd-lr-no text-left">
                                        <img src="dist/img/calender.png">
                                        <span>January 2018</span>
                                    </div>
                                    <!-- <div class="col-xs-6 col-sm-6 padd-lr-no text-right">
                                    <a href="dist/img/jan.pdf" target="_blank" download><img src="dist/img/download.png"></a>
                                </div> -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- <img class="earing" src="dist/img/newsletter-earing.png" alt="earing"> -->
            </div>
        </div>
    </section>



    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>





</body>

</html>
