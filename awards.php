<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <!--loader-->
    <div class="page-loader">
        <img src="dist/img/page-loader.gif" class="img-responsive loader">
    </div>
    <!--loader ends-->
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="awards" class="main_wrapper">
        <div class="container awards-section">
            <div class="row">
                <div class="col-md-12 heading">
                    <h2 class="">Awards and Accolades</h2>
                    <h2 class="timeline-year">2019</h2>
                    <p>BJS – 3rd Winner for Best Jewellery
                        <br>National Jewellery Award – Finalist for Colour Stone Jewellery</p>

                    <h2 class="timeline-year">2018</h2>
                    <p>Jewellers Choice Design Award – Winner for Couture Jewellery
                        <br>Jewellers Choice Design Award – Finalist for Coloured Stone Jewellery Coloured Stone Jewellery
                        <br>Jewellers Choice Design Award – Finalist for Kundan / Meena Jadau Jewellery
                        <br>GJC – Preferred Manufacturer of India<br>
                        PMI – Winner for Diamond Bridal Jewellery</p>

                    <h2 class="timeline-year">2017</h2>
                    <p>Jewellers Choice Design Award -Finalist for Colour Stone Jewellery</p>

                    <h2 class="timeline-year">2015</h2>
                    <p>National Jewellery Award – Winner for Best Jadau Jewellery</p>

                    <h2 class="timeline-year">2014</h2>
                    <p>Trendsetter Manufacturer by Retail Jeweller India</p>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>
</body>

</html>
