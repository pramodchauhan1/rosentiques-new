<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->

  	<section id="newsletter" class="main_wrapper">
  		<div class="container">
  				<div class="heading text-center">
  					<h2>rosentiques edge</h2>  
  				</div>

          <div class="sliderWRapper">
            <img class="bracelet" src="dist/img/bracelet.png">
            <div class="newsletter_wrapper">
              <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                <div class="newsSlider">
                <div class="item">
                  <img src="dist/img/news1.png" class="img-responsive">
                  <div class="date-download">
                    <h3>Issue 1</h3>
                    <div class="col-xs-12 padd-lr-no">
                      <div class="col-xs-6 col-sm-6 padd-lr-no text-left">
                        <img src="dist/img/calender.png">
                        <span>January</span>
                      </div>
                      <div class="col-xs-6 col-sm-6 padd-lr-no text-right">
                        <a href="dist/img/news1.png" target="_blank" download><img src="dist/img/download.png"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <img src="dist/img/news1.png" class="img-responsive">
                  <div class="date-download">
                    <h3>Issue 1</h3>
                    <div class="col-xs-12 padd-lr-no">
                      <div class="col-xs-6 col-sm-6 padd-lr-no text-left">
                        <img src="dist/img/calender.png">
                        <span>January</span>
                      </div>
                      <div class="col-xs-6 col-sm-6 padd-lr-no text-right">
                        <a href="dist/img/news1.png" target="_blank" download><img src="dist/img/download.png"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <img src="dist/img/news1.png" class="img-responsive">
                  <div class="date-download">
                    <h3>Issue 1</h3>
                    <div class="col-xs-12 padd-lr-no">
                      <div class="col-xs-6 col-sm-6 padd-lr-no text-left">
                        <img src="dist/img/calender.png">
                        <span>January</span>
                      </div>
                      <div class="col-xs-6 col-sm-6 padd-lr-no text-right">
                        <a href="dist/img/news1.png" target="_blank" download><img src="dist/img/download.png"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <img src="dist/img/news1.png" class="img-responsive">
                  <div class="date-download">
                    <h3>Issue 1</h3>
                    <div class="col-xs-6 padd-lr-no">
                      <div class="col-xs-12 col-sm-6 padd-lr-no text-left">
                        <img src="dist/img/calender.png">
                        <span>January</span>
                      </div>
                      <div class="col-xs-6 col-sm-6 padd-lr-no text-right">
                        <a href="dist/img/news1.png" target="_blank" download><img src="dist/img/download.png"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div> 
            </div>
            <img class="earing" src="dist/img/newsletter-earing.png">
          </div>
  		</div>
  	</section>



    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

      



</body>

</html>
